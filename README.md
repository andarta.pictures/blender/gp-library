# GP Library

Template system for Grease-pencil traditionnal to cut out animation.

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-library/-/archive/master/gp-library-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide

- This template system is dedicated to grease pencil mutli-object cut-out rigs, ordered in a parentship / collection hierarchy like the following example :

![guide](docs/gplib_01.png "guide")

- # Asset marking

![guide](docs/gplib_menu.png "guide")

- in the Asset Browser context menu, look for GP library Export operator :

![guide](docs/gplib_mark_asset.PNG "guide")

- Name your asset and defin it's frame range

- Posings : will create a pose asset for each frame in the frame range (adding its scene frame number as a suffix)

- Animation : create an animation asset for the whole frame range

- Make sure the active collection matches the animation level you want to define as an asset.

- You will also need a Camera in your scene in order to generate previews of your assets


- # Importing template :

- You can import a pose or animation by selecting one and clicking 'Import selected Asset' in the Asset browser context menu. (It should appear only if you selected a GP library asset)

- The add-on will look for correspondancy between it's reference collection, and your active collection's hierarchy in order to paste the pose on the right objects. It will create missing objects / collections.

- Note that it's will maintain parentship, modifiers and constraints when creating new objects but will not update if the scene and asset rigs don't match exactly. 

![guide](docs/gplib_keep_transforms.png "guide")

- The 'keep transforms' option allows you to apply the pose or animation using the position of the objects in the destination scene. I recommend to keep it enabled when importing poses for a whole character, and disabled when importing inside of the character's collection hierarchy.

# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)





