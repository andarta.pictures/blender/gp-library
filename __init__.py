# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


bl_info = {
    "name" : "GP library add-on",
    "author" : "Tom VIGUIER, Markus",
    "description" : "store and re-use GP poses and animation",
    "blender" : (3, 0, 1),
    "version" : (0, 0, 1),
    "location" : "3d view N-Panel",
    "warning" : "This tool is in early development stage, thank you for giving it a try ! please report any bug to t.viguier@andarta-pictures.com",
    "category" : "Andarta",
    "tracker_url": "https://gitlab.com/andarta-pictures/gp-library/-/issues",
}

import bpy
from mathutils import *
import math
import os
import shutil
import subprocess
import bpy.utils.previews
from bpy.props import (StringProperty,
                       PointerProperty,
                       )

from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )
from pprint import pprint
from bpy_extras.asset_utils import SpaceAssetInfo

def get_viewlayer_w_gp(context) :
    if len(context.scene.view_layers) > 1 and len([ob for ob in context.view_layer.objects if ob.type == 'GPENCIL']) == 0 :
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences 
        for vl in context.scene.view_layers :
            if vl.name == addon_prefs.vl_name :
                #print('A',vl)
                return vl
        for vl in context.scene.view_layers :    
            for ob in vl.objects :
                if ob.type == 'GPENCIL' :
                    #print('B',vl)
                    return vl
    else :
        #print('C',context.view_layer)
        return context.view_layer


def refresh_dopesheets():
    #print('::::::::::::dopesheets refresh :::::::::')
    context = bpy.context
    vl = context.window.view_layer
    view_layer = get_viewlayer_w_gp(context)
    context.window.view_layer = view_layer
    if context.object is None or context.object.type != 'GPENCIL':
        gp = get_any_gp()
        if gp is None and len(context.scene.collection.all_objects) > 0:
            view_layer = get_viewlayer_w_gp(context)
            view_layer.objects.active = context.scene.collection.all_objects[0]
    refresh_dopesheet_mode('GPENCIL')
    refresh_dopesheet_mode('DOPESHEET')
    context.window.view_layer = vl

def get_any_gp() :
    gpencils = []
    view_layer = get_viewlayer_w_gp(bpy.context)
    for obj in view_layer.objects :
        if obj.type == 'GPENCIL' :
            gpencils.append(obj)
    if not gpencils :
        return None
    print('________',bpy.context.view_layer.name)
    gpencils[0].select_set(True)
    view_layer.objects.active = gpencils[0]
    return gpencils[0]

def refresh_dopesheet_mode(mode) :
    #dirty way to force blender to refresh frames indices in a specific mode of dopesheet
    if bpy.context.object is None or (mode == 'GPENCIL' and bpy.context.object.type != 'GPENCIL'):
        return
    cur_areatype = str(bpy.context.area.type)
    bpy.context.area.type = 'DOPESHEET_EDITOR'
    cur_space_mode = str(bpy.context.area.spaces[0].mode)        
    bpy.context.area.spaces[0].mode = mode
    cur_show_only_selected = bpy.context.area.spaces[0].dopesheet.show_only_selected
    bpy.context.area.spaces[0].dopesheet.show_only_selected = False

    bpy.ops.action.select_all(action='SELECT')
    bpy.ops.action.mirror(type = 'XAXIS')
    bpy.ops.action.mirror(type = 'XAXIS')
    
    bpy.context.area.spaces[0].dopesheet.show_only_selected = cur_show_only_selected
    bpy.context.area.spaces[0].mode = cur_space_mode
    bpy.context.area.type = cur_areatype
    #print('refreshed '+mode+' of dopesheet editor')

def get_kf(frame_nb, layer):
    frames = []
    for fr in layer.frames:
        if fr.frame_number == frame_nb:
            frames.append(fr)
    return frames

def colormatch(src_ob, src_frame, ob, new_frame ) :
    #print('colormatching')
    #check if the stroke uses same material as source stroke 
    #assign the good material to each stroke (assume that all needed materials are already in gp_ob.materials)
    
    matched_strokes = []
    for i, stroke in enumerate(new_frame.strokes):
        #stroke_mat = gp_ob.materials[stroke.material_index] #WRONG : doesn't work if there are empty slots
        src_stroke = src_frame.strokes[i]

        try :
            stroke_mat = ob.material_slots[stroke.material_index].material
        except IndexError :
            stroke_mat = None

        #src_stroke_mat = src_gp_ob.materials[src_stroke.material_index]
        src_stroke_mat = src_ob.material_slots[src_stroke.material_index].material

        if src_stroke_mat :
            if stroke_mat == None or stroke_mat.name != src_stroke_mat.name[1:] :
                #print('color dont match ! ' + src_stroke_mat.name + ' - ' + stroke_mat.name)
                matched_strokes.append(stroke)

                for index, material_slot in enumerate(ob.material_slots) :
                    material = material_slot.material
                    if material is not None :
                        if material.name == src_stroke_mat.name or material.name == src_stroke_mat.name[1:] :
                            stroke.material_index = index
                            #print(str(i) + ' - colormatching : ' + src_stroke_mat.name + ' - ' +  material.name )
        if src_stroke_mat == None :
            stroke.material_index = -1
    #print('relocated ' + str(len(matched_strokes)) + ' strokes material')                    
    return {'FINISHED'}

def look_for_collection(collection, collection_name): 
    #print('look for ', collection_name, ' in ', collection.name)
    for coll in collection.children : 
        #print(coll.name)    
        if coll.name == collection_name :           
            return coll 
        else :
            coll = look_for_collection(coll, collection_name) 
            if coll :
                return coll   
    return None

def get_main_collections(src_scene_collection, active_collection, template_collection_name):
    #print('get_main_collections()')
    template_collection = look_for_collection(src_scene_collection, '_' + template_collection_name)
    if template_collection :
        if active_collection.name == template_collection_name : #template collection matches active collction
            #print('template collection matches active collction')
            return active_collection, template_collection
        else :
            main_collection = look_for_collection(active_collection, template_collection_name)
            if main_collection : #template collection matches one of active collection's children
                #print('template collection matches one of active collections children')
                return main_collection, template_collection
            else :
                main_src_collection = look_for_collection(src_scene_collection, '_' + active_collection.name)
                if main_src_collection : #active collection matches one of template collection's children
                    #print('active collection matches one of template collections children')
                    return active_collection, main_src_collection
                else : #no match, create a new collection
                    #print('no match, create new collection')
                    new_collection = bpy.data.collections.new(template_collection_name)
                    active_collection.children.link(new_collection)

                    return new_collection, template_collection
    else :
        #print('Asset description error (collection)')
        return None, None

def get_active_gp_frame(layer, frame_number) : 
    index = 0
    for i, frame in enumerate(layer.frames) :
        index = i 
        if frame.frame_number > frame_number :
            return layer.frames[i-1]
    return layer.frames[index]

def insert_LRS_keyframe(obj, frame_number) :
    #insert keyframes for location, rotation_euler and scale properties in obj at frame_number
    obj.keyframe_insert(data_path = 'location', index = -1, frame = frame_number)
    obj.keyframe_insert(data_path = 'rotation_euler', index = -1, frame = frame_number)
    obj.keyframe_insert(data_path = 'scale', index = -1, frame = frame_number)

def import_with_subcollections(main_src_collection, main_collection, root_objects, updated) :
    #check correspondancy at collection level, iterates import_in_col through subcollections)

    import_in_col(main_src_collection, main_collection, root_objects, updated)
    
    if main_src_collection.children is not None :
        for src_collection in main_src_collection.children :
            
            found_col = False
            for collection in main_collection.children : 
                if collection.name == src_collection.name[1:] :
                    
                    found_col = True
                    #print('-------')
                    #print('collection found : ' + collection.name)

                    import_with_subcollections(src_collection, collection, root_objects, updated)
            
            if found_col == False :
                #print('-------')
                #print('collection ' + src_collection.name[1:] + ' created ')
                
                collection = bpy.data.collections.new(src_collection.name[1:])
                collection.color_tag = src_collection.color_tag
                main_collection.children.link(collection)
                
                import_with_subcollections(src_collection, collection, root_objects, updated)

def sync_kf_attr(new_keyframe_point, src_keyframe_point) :
    new_keyframe_point.type = src_keyframe_point.type 
    new_keyframe_point.interpolation = src_keyframe_point.interpolation
    
    new_keyframe_point.easing = src_keyframe_point.easing
    new_keyframe_point.handle_right_type = src_keyframe_point.handle_right_type
    new_keyframe_point.handle_left_type = src_keyframe_point.handle_left_type
    new_keyframe_point.back = src_keyframe_point.back
    new_keyframe_point.amplitude = src_keyframe_point.amplitude
    new_keyframe_point.period = src_keyframe_point.period

def import_anim_data(src_ob, ob, root_objects) :
    scene = bpy.context.scene
    time = scene.frame_current

    #check existing animation
    if src_ob.animation_data is not None :
        #print('*/*/*/*/*/*/import anim data - time : ', time)
        if ob.animation_data is None :
            #print('create anim_data')
            ob.animation_data_create()

        if ob.animation_data.action is None :
            #print('create Action')
            ob.animation_data.action = bpy.data.actions.new(name = ob.name + 'Action')

        animloc = False
        animrot = False
        animsca = False
        for fcurve in ob.animation_data.action.fcurves :
            if fcurve.data_path == 'location' :
                animloc = True
            if fcurve.data_path == 'rotation_euler' :
                animrot = True
            if fcurve.data_path == 'scale' :
                animsca = True
        
        if animloc == False :
            ob.keyframe_insert(data_path = 'location', frame = 1, index = -1, group = 'Object Transforms')
            #print('keyframe location at frame 1')
        if animrot == False :
            ob.keyframe_insert(data_path = 'rotation_euler', frame = 1, index = -1, group = 'Object Transforms')
            #print('keyframe rotation at frame 1')
        if animsca == False :
            ob.keyframe_insert(data_path = 'scale', frame = 1, index = -1, group = 'Object Transforms')
            #print('keyframe scale at frame 1')

        #list src_fcurves and corresponding fcurves :
        fcurves_list = []
        for src_fcurve in src_ob.animation_data.action.fcurves :
            found = False
            for fcurve in ob.animation_data.action.fcurves :
                if fcurve.data_path == src_fcurve.data_path and fcurve.array_index == src_fcurve.array_index :
                    found = True
                    if not fcurve.lock :
                        fcurves_list.append((src_fcurve, fcurve))  
                    break
            if found == False :
                #print("no corrsponding fcurve to src_fcurve in scene object", src_fcurve.data_path)
                src_fc_group = ''
                if src_fcurve.group :
                    src_fc_group = src_fcurve.group.name
                new_fcurve = ob.animation_data.action.fcurves.new(src_fcurve.data_path, index = src_fcurve.array_index, action_group = src_fc_group)
                fcurves_list.append((src_fcurve, new_fcurve))
        #print(fcurves_list)

        rotated_deltas = None 
        if scene.gplib_data.use_root_transform == True and ob.name in root_objects and scene.gplib_data.import_mode == 'ANIM' :    
            if src_ob.rotation_euler != ob.rotation_euler :

                src_scene = bpy.data.scenes['Import']
                src_ob_rot_inv = src_ob.rotation_euler.to_matrix()
                src_ob_rot_inv.invert()

                ob_rot_inv = ob.rotation_euler.to_matrix()
                ob_rot_inv.invert()

                if ob.scale[0] * ob.scale[1] * ob.scale[2] < 0 :
                    rotation_diff = src_ob.rotation_euler.to_matrix() @ ob_rot_inv
                if ob.scale[0] * ob.scale[1] * ob.scale[2] > 0 :    
                    rotation_diff = ob.rotation_euler.to_matrix() @ src_ob_rot_inv

                src_origin = Vector(src_ob.location)

                rotated_deltas = dict()
                for src_fcurve in src_ob.animation_data.action.fcurves :
                    for src_keyframe_point in src_fcurve.keyframe_points :
                        if src_keyframe_point.co.x not in rotated_deltas.keys() :
                            src_scene.frame_set(src_keyframe_point.co.x)
                            rotated_deltas[src_keyframe_point.co.x] = (Vector(src_ob.location) - src_origin) @ rotation_diff
                src_scene.frame_set(1)

        for a in fcurves_list :
            src_fcurve = a[0]
            fcurve = a[1]
            new_keyframe_point = None  
            if scene.gplib_data.use_root_transform == True :                
                if ob.name in root_objects :
                    if scene.gplib_data.import_mode == 'ANIM':
                        #print('!!!!!!!! ANIM')
                        # extract specific offset from root_objects
                        offset = 0.0
                        scale_ratio = ob.scale[fcurve.array_index]/src_ob.scale[fcurve.array_index]

                        if fcurve.data_path == 'location' :
                            offset = ob.location[fcurve.array_index]
                        elif fcurve.data_path == 'rotation_euler' :
                            offset = ob.rotation_euler[fcurve.array_index]
                            scale_ratio = 1 # don't apply scale ratio on rotations
                        elif fcurve.data_path == 'scale' :
                            offset = ob.scale[fcurve.array_index]
                        # insert new keyframe points, with updated values
                        src_origin = 0.0
                        
                        for src_keyframe_point in src_fcurve.keyframe_points :
                            if src_keyframe_point.co.x == 1.0 :                               
                                src_origin = src_keyframe_point.co.y
                                new_keyframe_point = fcurve.keyframe_points.insert(src_keyframe_point.co.x + time - 1.0 , offset)
                                sync_kf_attr(new_keyframe_point, src_keyframe_point)
                            else:
                                if rotated_deltas and fcurve.data_path == 'location' :
                                    delta = rotated_deltas[src_keyframe_point.co.x][fcurve.array_index]          
                                else :
                                    delta = src_keyframe_point.co.y - src_origin
                                scaled_delta = delta * scale_ratio
                                value = offset + scaled_delta
                                #print('********', ob.name , fcurve.data_path, src_keyframe_point.co.x + time - 1.0, '+++scale_ratio', scale_ratio )
                                new_keyframe_point = fcurve.keyframe_points.insert(src_keyframe_point.co.x + time - 1.0 , value)
                                sync_kf_attr(new_keyframe_point, src_keyframe_point)
                            #print('*-*')
                    else:
                        #print('!!!!!!!! POSE')
                        # extract specific offset from root_objects
                        offset = 0.0
                        if fcurve.data_path == 'location' :
                            offset = ob.location[fcurve.array_index]
                        elif fcurve.data_path == 'rotation_euler' :
                            offset = ob.rotation_euler[fcurve.array_index]
                        elif fcurve.data_path == 'scale' :
                            offset = ob.scale[fcurve.array_index]
                        # insert new keyframe points, with updated values
                        for src_keyframe_point in reversed(src_fcurve.keyframe_points) :
                            new_keyframe_point = fcurve.keyframe_points.insert(src_keyframe_point.co.x + time - 1.0 , offset)
                            sync_kf_attr(new_keyframe_point, src_keyframe_point)
                            #print('*-*')
                
                else : 
                    for src_keyframe_point in reversed(src_fcurve.keyframe_points) :
                        new_keyframe_point = fcurve.keyframe_points.insert(src_keyframe_point.co.x + time - 1.0 , src_keyframe_point.co.y)
                        sync_kf_attr(new_keyframe_point, src_keyframe_point)
            else : 
                for src_keyframe_point in reversed(src_fcurve.keyframe_points) :
                    new_keyframe_point = fcurve.keyframe_points.insert(src_keyframe_point.co.x + time - 1.0 , src_keyframe_point.co.y)
                    sync_kf_attr(new_keyframe_point, src_keyframe_point)
            
                #print('copied keyframe point - fr: ' + str(int(src_keyframe_point.co.x)) + ' --- ' + str(src_keyframe_point.co.y + root_ob_offset))
    else : 
        #print('----------------no anim data in template')
        ob.matrix_local = src_ob.matrix_local #@ root_ob_matrix
        
        if ob.animation_data is not None :   
            #print('--keyframe template position')
            insert_LRS_keyframe(ob, time)

def import_in_col(src_collection, collection, root_objects, updated):
    #check correspondancy and import useful gp_objects, materials, layers and frames from source collection
    time = bpy.context.scene.frame_current
    
    #compare objects
    for src_ob in src_collection.objects :

        if src_ob.type != 'GPENCIL':
            found_obj = False
            for ob in collection.objects :
                if src_ob.name[1:] == ob.name :
                    #object exists :
                    found_obj = True
                    #print('----------------------------- object ' + ob.name + ' found')
                    #copy animation data
                    import_anim_data(src_ob, ob, root_objects)

            #### object is missing
            if found_obj == False :
                #remove potential remaining data even when object does not exist anymore (to avoid duplicated object problem)
                data_location = get_data_object_location(src_ob) #find the place to look for remaining data
                if src_ob.data is not None and data_location is not None:
                    name_data = src_ob.data.name[1:]
                    if name_data in data_location.keys() and data_location[name_data].users == 0:
                        data_location.remove(data_location[name_data])
                if src_ob.animation_data is not None and src_ob.animation_data.action is not None :
                    name_act = src_ob.animation_data.action.name[1:]
                    if name_act in bpy.data.actions.keys() and bpy.data.actions[name_act].users == 0:
                        bpy.data.actions.remove(bpy.data.actions[name_act])
                #print('----------------------------------------create object')

                #new_ob = bpy.data.objects.new(src_ob.name[1:] , src_ob.data)
                new_ob = src_ob.copy()
                new_ob.name = src_ob.name[1:]

                if new_ob.animation_data :
                    new_ob.animation_data_clear()

                if src_ob.data is not None :
                    src_ob.data.name = src_ob.data.name[1:]
                new_ob.empty_display_size = src_ob.empty_display_size
                new_ob.empty_display_type = src_ob.empty_display_type
                collection.objects.link(new_ob)
                ##restore animation data
                import_anim_data(src_ob, new_ob, root_objects)

            ##delete template objects (keep data)
            bpy.data.objects.remove(src_ob)

        else:
            src_gp_ob = src_ob.data
            found_obj = False
            for ob in collection.objects :
                if ob.type == 'EMPTY': #skip empty objects
                    continue
                gp_ob = ob.data

                if src_gp_ob.name[1:] == gp_ob.name :
                    #object exists :
                    found_obj = True
                    #print('-----------------------------gp object ' + gp_ob.name + ' found')

                    ####look for missing materials :
                    #get source colours
                    for src_material in src_gp_ob.materials :
                        if src_material is not None :

                            if src_material.name.startswith('_') :
                                src_material_name_ok = src_material.name[1:]
                            else :
                                src_material_name_ok = src_material.name

                            ###check in the destination object :
                            mat_in_gpob = False
                            for material in gp_ob.materials :
                                if material is not None :
                                    if src_material_name_ok == material.name :
                                        mat_in_gpob = True

                            ### if he's not, check in data :
                            if mat_in_gpob == False :
                                mat_in_data = False
                                for material in bpy.data.materials :
                                    #he's there, append it to destination object !
                                    if src_material_name_ok == material.name :
                                            mat_in_data = True
                                            gp_ob.materials.append(material)
                                            #print('missing material ' + material.name + ' added from data materials')

                                #he's nowhere but in the template, so let's rename and append it
                                if mat_in_data == False :
                                    gp_ob.materials.append(src_material)
                                    src_material.name = src_material_name_ok
                                    #print('missing material ' + src_material.name + ' added from template')

                    ####compare layers
                    for src_layer in src_gp_ob.layers :
                        #check for missing layers
                        found_lay = False
                        for layer in gp_ob.layers :
                            if src_layer.info == layer.info :
                                found_lay = True
                                #print('---layer ' + layer.info + ' found')

                        if found_lay == False :
                            #print('---create gp layer ' + src_layer.info)
                            gp_ob.layers.new(src_layer.info)

                        for layer in gp_ob.layers :
                            if src_layer.info == layer.info :
                                for src_frame in src_layer.frames :

                                    # check if there's already frame in destination and delete it
                                    frames = get_kf((src_frame.frame_number + time - 1), layer)
                                    for f in reversed(frames):
                                        layer.frames.remove(f)

                                    #copy frame from source
                                    new_frame = layer.frames.copy(src_frame)
                                    new_frame.frame_number = src_frame.frame_number + time - 1
                                    #print('frame copied at ' + str(new_frame.frame_number))

                                    #color matching
                                    #print('------ -- check colors')
                                    colormatch(src_ob, src_frame, ob, new_frame )

                                updated.append(layer) # layer updated, so stores it in the list

                    #copy animation data
                    import_anim_data(src_ob, ob, root_objects)

                    #update mask layers
                    for src_layer in src_gp_ob.layers :
                        for mask in src_layer.mask_layers :
                            if not gp_ob.layers[src_layer.info].mask_layers.get(mask.name) :
                                gp_ob.layers[src_layer.info].mask_layers.add(gp_ob.layers[mask.name])
                            gp_ob.layers[src_layer.info].mask_layers[mask.name].hide = mask.hide
                            gp_ob.layers[src_layer.info].mask_layers[mask.name].invert = mask.invert
                        # mask = None # for debug, to prevent access violation exception

            #### object is missing
            if found_obj == False :
                #remove potential remaining data even when object does not exist anymore (to avoid duplicated object problem)
                name_gp = src_gp_ob.name[1:]
                if name_gp in bpy.data.grease_pencils.keys() and bpy.data.grease_pencils[name_gp].users == 0:
                    bpy.data.grease_pencils.remove(bpy.data.grease_pencils[name_gp])
                if src_ob.animation_data is not None and src_ob.animation_data.action is not None :
                    name_act = src_ob.animation_data.action.name[1:]
                    if name_act in bpy.data.actions.keys() and bpy.data.actions[name_act].users == 0:
                        bpy.data.actions.remove(bpy.data.actions[name_act])

                #print('----------------------------------------create gp object')
                #new_ob = bpy.data.objects.new(src_ob.name[1:] , src_gp_ob)
                new_ob = src_ob.copy()
                new_ob.name = src_ob.name[1:]

                if new_ob.animation_data :
                    new_ob.animation_data_clear()
                    #new_ob.animation_data_create()

                new_name = src_gp_ob.name[1:]
                src_gp_ob.name = new_name

                collection.objects.link(new_ob)

                #handle materials
                #print('------------check materials-----' + new_ob.name)
                for i, src_material in enumerate(src_gp_ob.materials) :
                    if src_material is not None :
                        
                        if src_material.name.startswith('_') :
                            #print('----')
                            #print('look for ' + src_material.name[1:])
                            
                            found_mat = False
                            #check if material is already in the scene
                            for material in bpy.data.materials :
                                if src_material.name[1:] == material.name :
                                    found_mat = True
                                    #print('--found ' + material.name )
                                    #found it ! replace _material by material
                                    src_gp_ob.materials[i] = material

                            #nope, you need to rename and use the template material
                            if found_mat == False :
                                #print('----')
                                new_name = src_material.name[1:]
                                #print(src_material.name + ' renamed in ' + new_name)
                                src_material.name = new_name

                #offset frames to current time
                for layer in src_gp_ob.layers :
                    for frame in layer.frames :
                        frame.frame_number += time - 1

                # new object created so all its layer have been updated, stores them in the list :
                for layer in new_ob.data.layers:
                    updated.append(layer)

                ##restore animation data
                import_anim_data(src_ob, new_ob, root_objects)

            if found_obj == False :
                ##delete template objects (keep data)
                bpy.data.objects.remove(src_ob)

            else :
                #delete template data
                #print('delete src obj')
                bpy.data.grease_pencils.remove(src_gp_ob)

def insert_blank_keyframes(collection, updated, frame_number) :
    # insert blank keyframes on all layers in collection and not in updated. Keyframes inserted at frame_number.
    for obj in collection.objects :
        if obj.type == 'GPENCIL' :
            for layer in obj.data.layers :
                if layer not in updated :
                    frames = get_kf(frame_number, layer)
                    if frames:
                        # remove potential existing keyframe
                        for f in reversed(frames):
                            layer.frames.remove(f)
                    layer.frames.new(frame_number) # insert blank keyframe

    for col in collection.children :
        insert_blank_keyframes(col, updated, frame_number)


def get_data_object_location(obj) :
    #return the location of datablocks in bpy.data according to the type of given object
    data_location = {
        'MESH' : bpy.data.meshes,
        'CURVE' : bpy.data.curves,
        'SURFACE' : bpy.data.curves,
        'META' : bpy.data.metaballs,
        'FONT' : bpy.data.curves,
        'HAIR' : bpy.data.particles,
        'VOLUME' : bpy.data.volumes,
        'GPENCIL' : bpy.data.grease_pencils,
        'ARMATURE' : bpy.data.armatures,
        'LATTICE' : bpy.data.lattices,
        'EMPTY' : bpy.data.objects,
        'LIGHT' : bpy.data.lights,
        'LIGHT_PROBE' : bpy.data.lightprobes,
        'CAMERA' : bpy.data.cameras,
        'SPEAKER' : bpy.data.speakers
        }
    return data_location.get(obj.type)

def clean_datablocks(prefix):
    #delete all data-blocks beginning with given prefix
    object_types = [
        bpy.data.materials,
        bpy.data.actions,
        bpy.data.grease_pencils,
        bpy.data.meshes,
        bpy.data.curves,
        bpy.data.metaballs,
        bpy.data.volumes,
        bpy.data.armatures,
        bpy.data.lattices,
        bpy.data.lights,
        bpy.data.lightprobes,
        bpy.data.cameras,
        bpy.data.speakers,
        bpy.data.collections,
        bpy.data.objects
        ]
    #iterate on each data blocks location
    for data_location in object_types :
        for elem in data_location :
            if elem.name.startswith(prefix) :
                #print('remove ' + elem.name)
                data_location.remove(elem)

#####################################
#-----------PROPERTIES--------------#
#####################################
class GPLIB_PROPS(PropertyGroup):
    bl_label = "GP Library properties"
    bl_idname = "gplib.properties"
    #import settings
    use_root_transform : bpy.props.BoolProperty(
        name = 'keep transforms',
        description = 'Use local objects transformations' , 
        default = False)
    root_objects = []
    #export settings
    filename_export : bpy.props.StringProperty()
    export_range_start : bpy.props.IntProperty()
    export_range_stop : bpy.props.IntProperty()

    export_poses : bpy.props.BoolProperty(default = False)
    export_anim : bpy.props.BoolProperty(default = True)

    import_mode : bpy.props.StringProperty(default = 'ANIM')

def new_gplib_asset(name, description):
    new_asset = bpy.data.actions.new(name)
    new_asset.asset_mark()
    new_asset.asset_data.description = description
    return new_asset

class GPLIB_OT_CREATE_ASSET(bpy.types.Operator):     
    bl_label = "Mark pose or animation as asset"
    bl_idname = "gplib.mark_asset"

    use_scene_camera : bpy.props.BoolProperty(default = True)
    cancel : bpy.props.BoolProperty(default = False)
    def invoke(self, context, event):
        #print('invoke')
        wm = context.window_manager
        self.gplib_data = context.scene.gplib_data
        self.gplib_data.export_range_start = context.scene.frame_current
        self.gplib_data.export_range_stop = context.scene.frame_current
        
        self.init_time = context.scene.frame_current
        self.init_scene = context.scene

        return wm.invoke_props_dialog(self) 
    
    def draw(self,context):
        layout = self.layout
        layout.use_property_split = True
        split=layout.split()
        col=split.column()
        if context.scene.camera :
            #col.prop(self.gplib_data, 'thumbs_rez', text = 'Thumbnails resolution')
            col.prop(self.gplib_data, 'filename_export', text = 'Template name')
            col.prop(self.gplib_data, 'export_range_start', text = 'From frame')
            col.prop(self.gplib_data, 'export_range_stop', text = 'To frame')

            #Export range : start cant be > stop

            col.prop(self.gplib_data, 'export_poses', text = 'posings')
            col.prop(self.gplib_data, 'export_anim', text = 'animation')
            col.label(text = 'active collection :' + context.collection.name)
            #col.prop(self, 'use_scene_camera', text = 'Use scene camera')
            #col.prop(self.gplib_data, 'zero_root_objects', text = 'zero initial transforms')
        else :
            col.label(text = 'You need a camera in the scene to frame thumbnails')
            self.cancel = True

    def execute(self, context):

        #print('#------------------------------------------------------#')
        #print('#-------------start mark asset script------------------#')
        #print('#------------------------------------------------------#')
        if not self.cancel :
            init_cur_frame = context.scene.frame_current
            collection = context.collection.name
            if self.gplib_data.export_poses :
                mode = 'POSE'
                for frame_number in range(self.gplib_data.export_range_start, self.gplib_data.export_range_stop +1):  
                    time_code = str(frame_number).zfill(4)
                    name = self.gplib_data.filename_export + '-' + str(frame_number)
                    description = 'GPLIB_' + mode + '-' + time_code  + '-xxxx-' + collection
                    new_asset = new_gplib_asset(name, description)
                    
                    context.scene.frame_current = frame_number
                    new_asset.asset_generate_preview()

            if self.gplib_data.export_anim :
                mode = 'ANIM'
                range_tc = str(self.gplib_data.export_range_start).zfill(4) + '-' + str(self.gplib_data.export_range_stop).zfill(4)
                name = self.gplib_data.filename_export 
                description = 'GPLIB_' + mode + '-' + range_tc + '-' + collection
                new_asset = new_gplib_asset(name, description)
                context.scene.frame_current = self.gplib_data.export_range_start
                new_asset.asset_generate_preview()

            context.scene.frame_current = init_cur_frame

            return {'FINISHED'}    
        else:
            return {'CANCELLED'}  

class GPLIB_OT_IMPORT(bpy.types.Operator):     
    bl_label = "import from bank"
    bl_idname = "gplib.importer"
    bl_options = {'REGISTER', 'UNDO'}
    
    root_objects = list()
    parents = []
    children = []
    matrix_parent_inverse = []
    layers_parentship = []

    filepath : bpy.props.StringProperty()
    mode : bpy.props.StringProperty()
    frame_number : bpy.props.IntProperty()
    frame_number_anim_end : bpy.props.IntProperty()
    template_collection_name : bpy.props.StringProperty()
    #use_root_transform : bpy.props.BoolProperty(description = 'Use root objects transformations' , default = False)

    def execute(self, context):
        
        #print('#------------------------------------------------------#')
        print('#----------------start import script-------------------#')
        #print('#------------------------------------------------------#')
        #print(context)
        #print('template file : ' + self.filepath)
        #make a temporary copy of the file
        split_path = self.filepath.split('\\')
        split_path[-1] = "temp_gplib_" + split_path[-1]
        copy_path = "\\".join(split_path)

        shutil.copy(self.filepath,copy_path)
        #modify the copy to ease import
        #print('copying file')
        #try:
        addon_folder = os.path.dirname(__file__)
        import_file_management_path = addon_folder +'\\import_file_management.py'
        
        subprocess.run(["blender", copy_path, "-b", "-P", import_file_management_path])

        scene = bpy.context.scene
        #print('importing copied file')
        #import template to bpy.data
        with bpy.data.libraries.load(copy_path) as (data_from, data_to):
            data_to.scenes = data_from.scenes


        #store import mode in data
        if self.mode == 'ANIM':
            scene.gplib_data.import_mode = 'ANIM'
        elif self.mode == 'POSE':
            scene.gplib_data.import_mode = 'POSE'

        #check if there's only one scene in template and get src_scene
        src_scene = data_to.scenes[0]
        src_scene.name = 'Import'
        if len(data_to.scenes) != 1 :
            #print('ERRORS MAY OCCUR - need only one scene in the template')
            pass

        main_collection, main_src_collection = get_main_collections(src_scene.collection, bpy.context.collection, self.template_collection_name)
        if main_collection and main_src_collection :
        #print('source collection : ' + main_src_collection.name)
        #print('---')
        
        ## find root objects  :
            root_objects = []
            main_src_coll_objs = []
            for src_ob in main_src_collection.all_objects :
                main_src_coll_objs.append(src_ob.name)

            for src_ob in main_src_collection.all_objects :
                if src_ob.parent == None :
                    root_objects.append(src_ob.name[1:])
                else :
                    if src_ob.parent.name not in main_src_coll_objs :
                        root_objects.append(src_ob.name[1:])
            
            #return {'FINISHED'} # TEST STOP

            #keyframes column at self.frame_number
            frame_number = self.frame_number
            frame_number_end = self.frame_number_anim_end

            src_scene.frame_set(frame_number)

            #switch temporarily to src_scene to refresh dopesheet to update the "active frame"     
            #save current 3D view
            view_perspective = 'ORTHO' #default value in blender
            for area in bpy.context.screen.areas :
                if area.type == 'VIEW_3D':
                    for space in area.spaces :
                        if space.type == 'VIEW_3D':
                            view_perspective = space.region_3d.view_perspective
                            break
                    break
            
            #refresh
            bpy.context.window.scene = src_scene
            refresh_dopesheets()
            
            #return {'FINISHED'} # TEST STOP
            for src_ob in main_src_collection.all_objects :

                insert_LRS_keyframe(src_ob, frame_number)

                #delete other object keyframes
                for src_fcurve in src_ob.animation_data.action.fcurves :
                    kf_trash = []
                    for src_keyframe_point in src_fcurve.keyframe_points :
                        if src_keyframe_point.co.x < frame_number or src_keyframe_point.co.x > frame_number_end  :
                            kf_trash.append(src_keyframe_point)

                    for kf in reversed(kf_trash) :
                        src_fcurve.keyframe_points.remove(kf)

                #GP keyframes :
                if src_ob.type == 'GPENCIL':
                    src_gp_ob = src_ob.data
                    for layer in src_gp_ob.layers :
                        frame_exists = False
                        for frame in layer.frames :
                            if frame.frame_number == frame_number :
                                frame_exists = True

                        if frame_exists == False :
                            new_fr = layer.frames.copy(get_active_gp_frame(layer, frame_number))
                            new_fr.frame_number = frame_number

                        #delete other frames
                        for frame in layer.frames :
                            if frame.frame_number < frame_number or frame.frame_number > frame_number_end :
                                layer.frames.remove(frame)

                    #move frames to start at frame 1                
                    for layer in src_gp_ob.layers :
                        for frame in layer.frames :
                            frame.frame_number -= frame_number - 1

                #move keyframe column to frame 1
                for src_fcurve in src_ob.animation_data.action.fcurves :
                    for src_keyframe_point in src_fcurve.keyframe_points :
                        src_keyframe_point.co.x -= frame_number - 1

            src_scene.frame_set(1)
            #return {'FINISHED'} #TEST STOP
            refresh_dopesheets()
            bpy.context.window.scene = scene
            
            #restore old 3D view
            for area in bpy.context.screen.areas :
                if area.type == 'VIEW_3D':
                    for space in area.spaces :
                        if space.type == 'VIEW_3D':
                            space.region_3d.view_perspective = view_perspective
                            break
                    break
            #return {'FINISHED'} #TEST STOP

            ####list source object and store parentship
            self.parents = []
            self.children = []
            self.matrix_parent_inverse = []
            self.layers_parentship = []

            #print('root objs')
            #print(root_objects)
            #print('source objects : ')
            for src_ob in main_src_collection.all_objects :
                #if src_ob.name[1:] in root_objects :
                    #print('*' + src_ob.name)
                #else :
                    #print(src_ob.name)

                if src_ob.parent is not None :
                    if src_ob.parent_type == 'OBJECT' :
                        self.children.append(src_ob.name[1:])
                        self.parents.append(src_ob.parent.name[1:])
                        self.matrix_parent_inverse.append(src_ob.matrix_parent_inverse.copy())
                if src_ob.type == 'GPENCIL' :
                    src_gp_ob = src_ob.data
                    for src_layer in src_gp_ob.layers :
                        if src_layer.parent and src_layer.parent_type == 'OBJECT':
                            self.layers_parentship.append((src_gp_ob.name, src_layer.info, src_layer.parent.name, Matrix(src_layer.matrix_inverse)))
            #print(self.layers_parentship)

            updated = [] #list to store all layers which are updated with this import
            import_with_subcollections(main_src_collection, main_collection, root_objects, updated)
            #return {'FINISHED'} # TEST STOP
            #restore parentship
            #print('-----------------parentships----')
            for item in self.layers_parentship :
                gp_ob_name = item[0]
                layer_name = item[1] 
                parent_name= item[2] 
                matrix_inverse = item[3]
                if gp_ob_name[:1] == '_' :
                    gp_ob_name = gp_ob_name[1:]
                if parent_name[:1] == '_' :
                    parent_name = parent_name[1:]
                parent = bpy.data.objects[parent_name]
                layer = bpy.data.grease_pencils[gp_ob_name].layers[layer_name]
                #print(matrix_inverse)
                layer.parent = parent
                layer.matrix_inverse = matrix_inverse
                #print('parented layer', layer.id_data, layer, 'to', parent.name)
                #print(layer.matrix_inverse)

            if self.parents is not None :
                for i in range(len(self.parents)) :
                    child = None
                    parent = None
                    for obj in main_collection.all_objects :
                        if obj.name == self.children[i] :
                            child = obj
                        if obj.name == self.parents[i] :
                            parent = obj
                    if child is not None :
                        if parent is not None :
                            child.parent = parent
                            child.matrix_parent_inverse = self.matrix_parent_inverse[i]
                            #print('parented ' + child.name + ' to ' + parent.name)

            # insert gp blank keyframes on objects not present in template
            insert_blank_keyframes(main_collection, updated, scene.frame_current)
            #return {'FINISHED'} # TEST STOP
            #print('-----------clean up -----------')
            clean_datablocks('_')

            for scene in data_to.scenes :
                #print('remove scene : ' + scene.name)
                bpy.data.scenes.remove(scene)

            for lib in bpy.data.libraries :
                if lib.filepath == copy_path :
                    #print('remove library : ' + lib.name)
                    bpy.data.libraries.remove(lib)      
            refresh_dopesheets()
            
            #delete the copy of file created at import beginning
            os.remove(copy_path)
            #force GP dopesheet to refresh :
            if 'ds_data' in dir(context.window_manager) and context.window_manager.ds_data.switch_on == True :
                bpy.ops.dope.maintain(order = 'FORCE_REDRAW_KEEP_SELECTED')
            #print('-------------------------end')
            return {'FINISHED'}   
        else :
            return {'CANCELLED'}   

class GPLIB_OT_IMPORT_ASSET(bpy.types.Operator):     
    bl_label = "Import gplib asset from Asset Browser"
    bl_idname = "gplib.import_asset"
    #bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context) :
        
        if bpy.context.area.ui_type == 'ASSETS':
            #print('---')
            asset = bpy.context.asset_file_handle
            asset_data = asset.asset_data
            if asset_data.description.startswith('GPLIB'):

                '''lib_folder = context.area.spaces[0].params.directory.decode('utf-8')
                asset_path = 'None'
                for i in range(5, len(asset.relative_path)):
                    if asset.relative_path[i:].startswith('Action/') :
                        asset_path = asset.relative_path[:i-1]
                        break    
                filepath = lib_folder + asset_path
                #print(filepath)'''

                filepath = context.window_manager.asset_path_dummy
                if filepath == 'Current File' :
                    filepath = bpy.data.filepath
                    if filepath == '' :
                        print('Error : you must save this blend file before importing from it')
                        return{'CANCELLED'}
                mode = asset_data.description[6:10]          
                #print(mode)
                #print('****pose_frame_number****' + asset_data.description[11:15])
                try :
                    frame_number_start = int(asset_data.description[11:15])
                    #print(frame_number_start)
                    if mode == 'ANIM' :
                        #print(asset_data.description[16:20])
                        frame_number_end = int(asset_data.description[16:20])
                    else :
                        frame_number_end = frame_number_start
                    #print(frame_number_end)
                    collection_name = asset_data.description[21:]
                    #print(collection_name)

                except ValueError :
                    #print('error in asset description format')
                    return {'CANCELLED'}
                #print('run import')
                bpy.ops.gplib.importer(filepath = filepath, mode = mode, frame_number = frame_number_start, frame_number_anim_end = frame_number_end, template_collection_name = collection_name )
        else : 
            #print('WRONG AREA CONTEXT')
            pass
        return {'FINISHED'}

def draw_import_context_menu(self, context):
    scene = context.scene
    asset = bpy.context.asset_file_handle
    if asset :
        asset_data = asset.asset_data
        if asset_data.description.startswith('GPLIB'):
            layout = self.layout
            layout.separator()
            row = layout.row()
            row.label(text = 'GP Library Import')
            row = layout.row()
            row.operator('gplib.import_asset', text = 'Import selected Asset', emboss=True, depress = False) 
            row = layout.row()
            row.prop(scene.gplib_data ,'use_root_transform')

def draw_export_context_menu(self, context):
    scene = context.scene
    layout = self.layout
    layout.separator()
    row = layout.row()
    row.label(text = 'GP Library Export')
    row = layout.row()
    row.operator_context = 'INVOKE_DEFAULT'
    row.operator('gplib.mark_asset', text = 'Create Assets in current file', emboss=True, depress = False)    


class GPLIB_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    vl_name : bpy.props.StringProperty(default = 'BUILD')
    

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text = 'GP Library preferences')
        row = layout.row()
        row.prop( self, 'vl_name', text = 'Name of the build viewlayer (with every collection active)')



classes = [GPLIB_OT_IMPORT,
    GPLIB_PROPS,
    GPLIB_OT_IMPORT_ASSET,
    GPLIB_OT_CREATE_ASSET,
    GPLIB_AddonPref
]               
                        
def register():
    print('-----------------------------------------------register')
    for cls in classes :
        bpy.utils.register_class(cls)   
    bpy.types.Scene.gplib_data = bpy.props.PointerProperty(type=GPLIB_PROPS)
    bpy.types.ASSETBROWSER_MT_context_menu.append(draw_import_context_menu)
    bpy.types.ASSETBROWSER_MT_context_menu.append(draw_export_context_menu)
 
def unregister():
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.gplib_data
    bpy.types.ASSETBROWSER_MT_context_menu.remove(draw_import_context_menu)
    bpy.types.ASSETBROWSER_MT_context_menu.remove(draw_export_context_menu)

if __name__ == "__main__":
    register()                        
