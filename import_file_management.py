import bpy
#print('import_file_management.py')

prefix = "_"
object_types = [
    bpy.data.materials,
    bpy.data.actions,
    bpy.data.grease_pencils,
    bpy.data.meshes,
    bpy.data.curves,
    bpy.data.metaballs,
    bpy.data.volumes,
    bpy.data.armatures,
    bpy.data.lattices,
    bpy.data.lights,
    bpy.data.lightprobes,
    bpy.data.cameras,
    bpy.data.speakers,
    bpy.data.collections,
    bpy.data.objects
    ]
#iterate on all data blocks to rename them
for data_location in object_types :
    for elem in data_location.values() :
        if not elem.library :
            elem.name = prefix + elem.name
            
#avoid empty gp layers            
for ob in bpy.context.scene.objects :
    if ob.type == 'GPENCIL' :
        gp = ob.data 
        for layer in gp.layers :
            if len(layer.frames) == 0 :
                layer.frames.new(0, active=False)

#save file
bpy.context.preferences.filepaths.save_version = 0
bpy.ops.wm.save_as_mainfile()
bpy.context.preferences.filepaths.save_version = 1